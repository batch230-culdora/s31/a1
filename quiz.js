/*
1. In the S31 folder, create an activity folder and an activity.js file inside of it.
2. Copy the questions provided by your instructor into the activity.js file.
3. The questions are as follows:
- What directive is used by Node.js in loading the modules it needs?
	// ans. the "require" directive

- What Node.js module contains a method for server creation?
	// ans.  The module is "HTTP module"


- What is the method of the http object responsible for creating a server using Node.js?
	// ans. The method is "createServer()"

- What method of the response object allows us to set status codes and content types?
	// ans. the "writeHead" method

- Where will console.log() output its contents when run in Node.js?
	// ans. Outputs from using console.log will be displayed to the terminal.


- What property of the request object contains the address's endpoint?
	// ans. "request.url"


/*